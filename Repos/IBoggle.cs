﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boggle.Repos
{
    public interface IBoggle
    {
        int WordListScore(string word);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Boggle.Repos;
using Boggle.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Boggle.Controllers
{
    public class BoggleController : Controller
    {
        private readonly IBoggle _bgl;

        public BoggleController(IBoggle bgl)
        {
            _bgl = bgl;
        }
        public IActionResult Result(WordVM model)
        {
            ResultVM fSend = new ResultVM();

            if (model.WordList.Count > 0)
            {
                foreach (var word in model.WordList)
                {
                    fSend.Score = _bgl.WordListScore(word);
                }
            }
            return View(fSend);
        }
        public IActionResult Randomizer()
        {
			var a;
            const int Row = 4;
            const int Col = 4;
            string[] letters = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"
                , "L", "M", "N", "O", "P", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            Random random = new Random();
            string[,] arr = new string[Row, Col];
            for (int i = 0; i < Row; i++)
            {
                for (int j = 0; j < Col; j++)
                {
                    arr[i, j] = letters[random.Next(0, letters.Count())];
                }
            }
            ViewData["Matrix"] = arr;

            return PartialView();
        }

        public IActionResult PlayersWordList(WordVM model)
        {
            return View();
        }

    }
}
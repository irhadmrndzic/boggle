﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Boggle.ViewModels
{
    public class WordVM
    {
        public List<string> PlayerName { get; set; }
        public List<string> WordList { get; set; }
    }
}

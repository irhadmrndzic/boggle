﻿using Microsoft.EntityFrameworkCore;

namespace Boggle.DATA
{
    public class BoggleDbContext:DbContext
    {
        public BoggleDbContext(DbContextOptions<BoggleDbContext> options) : base(options)
        {
        }
    }
}

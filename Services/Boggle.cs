﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Boggle.Repos;

namespace Boggle.Services
{
    public class Boggle:IBoggle
    {
        public int WordListScore(string nWords)
        {
            int score = 0;
            if (nWords.Length == 0 && !nWords.All(Char.IsLetter))
                return 0;

            foreach (var word in nWords.Split(','))
            {
                int counter = word.Length;

                if (counter <= 2)
                    score = 0;
                else if (counter == 3 || counter == 4)
                    score += 1;
                else if (counter == 5)
                    score += 2;
                else if (counter == 6)
                    score += 3;
                else if (counter == 7)
                    score += 5;
                else
                    score += 11;
            }
            return score;
        }

    }
}
